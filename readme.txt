Test: backendtest quick
Author: Olle Mårtensson ( Becausemondays )
Email: olle@becausemondays.com
Class implementing ScoreServer: com.king.backendtest.impl.ScoreServerImpl
Thoughts  & Comments:

There is a Test class com.king.backendtest.Tests ( src/test/java ) that tests the functionality of the system
and might also serve as a description of how the system works.

I was not sure about how the simple login-system should operate
so I created a separate authentication service that can be used
instead of com.king.backendtest.ScoreServer#getSessionKey to
start a new session and retrieve a session key, please see com.king.backendtest.AuthenticationServer for more details.

The com.king.backendtest.ScoreRepo class is the class containing
the datastructures that holds the topscore state.

The com.king.backendtest.SessionRepo class contains logic for
timouts of sessions etc.