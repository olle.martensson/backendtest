package com.king.backendtest;

import com.king.backendtest.api.AuthenticationServer;
import com.king.backendtest.api.ScoreServer;
import com.king.backendtest.api.System;
import com.king.backendtest.domain.NoSuchUserException;
import com.king.backendtest.domain.ScoreListEntry;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class Tests {

    System sys;

    @BeforeEach
    public void createSystem() {
        this.sys = Application.start();
    }

    @AfterEach
    public void stopSystem() {
        this.sys.stop();
    }

    @Test
    public void authenticationServer() {
        AuthenticationServer srv = this.sys.getAuthenticationServer();
        assertDoesNotThrow(() -> srv.authenticate("olle"));
        assertThrows(NoSuchUserException.class, () -> srv.authenticate("doesnotexist"));
    }

    @Test
    public void sessionTimeout() throws InterruptedException {
        System sys = Application.start(100, TimeUnit.MILLISECONDS);
        ScoreServer srv = sys.getScoreServer();
        String sessionKey = srv.getSessionKey(Integer.MAX_VALUE);
        assertDoesNotThrow(() -> srv.registerScore(sessionKey, 10, 100));
        Thread.sleep(100);
        assertThrows(IllegalArgumentException.class, () -> srv.registerScore(sessionKey, 10, 100));
        sys.stop();
    }

    @Test
    public void scoreServerTopList() throws InterruptedException {
        ScoreServer srv = this.sys.getScoreServer();

        CountDownLatch player1Work = new CountDownLatch(7);
        String player1SessionKey = srv.getSessionKey(1);
        IntStream.of(1, 2, 3, 4, 5, 6, 7).parallel().forEach((int score) -> {
            srv.registerScore(player1SessionKey, 1, score);
            player1Work.countDown();
        });

        CountDownLatch player2Work = new CountDownLatch(5);
        String player2SessionKey = srv.getSessionKey(2);
        IntStream.of(8, 9, 10, 11, 12).parallel().forEach((int score) -> {
            srv.registerScore(player2SessionKey, 1, score);
            player2Work.countDown();
        });

        CountDownLatch player3Work = new CountDownLatch(8);
        String player3SessionKey = srv.getSessionKey(3);
        IntStream.of(13, 14, 15, 16, 17, 18, 19, 20).parallel().forEach((int score) -> {
            srv.registerScore(player3SessionKey, 1, score);
            player3Work.countDown();
        });

        player1Work.await();
        player2Work.await();
        player3Work.await();

        ScoreListEntry[] topList = srv.getTopList(1);

        assertEquals(15, topList.length);

        int[] expectedToplistScores = {6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
        assertArrayEquals(expectedToplistScores, Arrays.stream(topList).mapToInt(ScoreListEntry::getScore).toArray());

        int[] expectedTopListUsers = {1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3};
        assertArrayEquals(expectedTopListUsers, Arrays.stream(topList).mapToInt(ScoreListEntry::getUserId).toArray());

    }

}
