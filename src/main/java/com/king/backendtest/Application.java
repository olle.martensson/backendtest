package com.king.backendtest;

import com.king.backendtest.api.AuthenticationServer;
import com.king.backendtest.api.ScoreServer;
import com.king.backendtest.api.System;
import com.king.backendtest.api.UserServer;
import com.king.backendtest.impl.*;

import java.util.concurrent.TimeUnit;

public class Application {

    public static System start() {
        return Application.start(10, TimeUnit.MINUTES);
    }

    public static System start(int timeout, TimeUnit timeoutUnit) {
        SessionRepo sessionRepo = new SessionRepo(timeout, timeoutUnit);
        ScoreRepo scoreRepo = new ScoreRepo();
        ScoreServer scoreServer = new ScoreServerImpl(sessionRepo, scoreRepo);
        UserServer userServer = new UserServerImpl();
        AuthenticationServer authenticationServer = new AuthenticationServerImpl(userServer, scoreServer);

        return new System() {
            @Override
            public ScoreServer getScoreServer() {
                return scoreServer;
            }

            @Override
            public AuthenticationServer getAuthenticationServer() {
                return authenticationServer;
            }

            @Override
            public void stop() { sessionRepo.stop(); }
        };
    }

    public static void main(String[] args) {
	// write your code here
    }
}
