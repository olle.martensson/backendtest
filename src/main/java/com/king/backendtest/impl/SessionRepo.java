package com.king.backendtest.impl;

import java.sql.Time;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SessionRepo {
    private Map<UUID, Integer> activeSessions;
    private ScheduledExecutorService executor;
    private int timeout;
    private TimeUnit timeoutUnit;

    public SessionRepo(int timeout, TimeUnit timeoutUnit) {
        this.timeout = timeout;
        this.timeoutUnit = timeoutUnit;
        this.activeSessions = new HashMap<>();
        this.executor = Executors.newSingleThreadScheduledExecutor();
    }

    private synchronized void timeoutSession(UUID sessionId) {
        this.activeSessions.remove(sessionId);
    }

    /**
     * @return the userId of the active session if any
     */
    public synchronized Optional<Integer> getSessionUser(UUID sessionId) {
        return Optional.ofNullable(this.activeSessions.get(sessionId));
    }

    /**
     * Creates a new session that times out after 10 min.
     */
    public synchronized UUID startSession(int playerId) {
        UUID sessionId = UUID.randomUUID();
        this.activeSessions.put(sessionId, playerId);

        this.executor.schedule(new SessionTimeoutTask(sessionId), this.timeout, this.timeoutUnit);

        return sessionId;
    }

    private class SessionTimeoutTask implements Runnable {
        private UUID sessionId;

        SessionTimeoutTask(UUID sessionId) {
            this.sessionId = sessionId;
        }

        @Override
        public void run() {
            timeoutSession(this.sessionId);
        }
    }

    public void stop() { this.executor.shutdown();}
}
