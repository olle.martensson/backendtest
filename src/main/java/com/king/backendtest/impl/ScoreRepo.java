package com.king.backendtest.impl;

import com.king.backendtest.api.Stoppable;
import com.king.backendtest.domain.ScoreListEntry;

import java.util.*;

public class ScoreRepo {
    private static int MAX_SCORES_PER_LEVEL = 15;
    private Map<Integer, TreeSet<ScoreListEntry>> scores;

    public ScoreRepo() {
        this.scores = new HashMap<>();
    }

    public synchronized void registerScore(int userId, int level, int score) {
        TreeSet<ScoreListEntry> levelScores = this.scores.getOrDefault(level, new TreeSet<>(new SortByScore()));
        levelScores.add(new ScoreListEntry(userId, score));

        if (levelScores.size() > MAX_SCORES_PER_LEVEL) {
            levelScores.pollFirst();
        }

        this.scores.put(level, levelScores);
    }

    public synchronized Optional<ScoreListEntry[]> getTopList(int level) {
        SortedSet<ScoreListEntry> topListSet = this.scores.get(level);

        if (topListSet == null) {
            return Optional.empty();
        }

        ScoreListEntry[] topList = new ScoreListEntry[topListSet.size()];

        return Optional.of(topListSet.toArray(topList));
    }

    static class SortByScore implements Comparator<ScoreListEntry> {
        @Override
        public int compare(ScoreListEntry o1, ScoreListEntry o2) {
            return Integer.compare(o1.getScore(), o2.getScore());
        }
    }
}
