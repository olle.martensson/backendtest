package com.king.backendtest.impl;

import com.king.backendtest.domain.ScoreListEntry;
import com.king.backendtest.api.ScoreServer;

import java.util.Optional;
import java.util.UUID;

/**
 * A not so production ready implementation of the {@link ScoreServer}
 */
public class ScoreServerImpl implements ScoreServer {
    private SessionRepo sessionRepo;
    private ScoreRepo scoreRepo;

    public ScoreServerImpl(SessionRepo sessionRepo, ScoreRepo scoreRepo) {
        this.sessionRepo = sessionRepo;
        this.scoreRepo = scoreRepo;
    }

    @Override
    public String getSessionKey(int playerId) {
        return this.sessionRepo.startSession(playerId).toString();
    }

    @Override
    public ScoreListEntry[] getTopList(int level) {
        return this.scoreRepo.getTopList(level).orElseGet(() -> new ScoreListEntry[]{});
    }

    @Override
    public void registerScore(String sessionKey, int level, int score) {
        UUID sessionId = UUID.fromString(sessionKey); // throws IllegalArgumentException if not UUID
        Optional<Integer> userIdOption = sessionRepo.getSessionUser(sessionId);
        int userId = userIdOption.orElseThrow(() -> new IllegalArgumentException("Not an active session"));
        scoreRepo.registerScore(userId, level, score);
    }
}
