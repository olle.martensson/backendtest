package com.king.backendtest.impl;

import com.king.backendtest.api.UserServer;
import com.king.backendtest.domain.NoSuchUserException;
import com.king.backendtest.domain.User;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

public class UserServerImpl implements UserServer {
    private static final Map<String, Integer> users = Collections.singletonMap("olle", 1);

    @Override
    public Optional<User> getUserByNick(String nick) throws NoSuchUserException {
        return Optional.ofNullable(users.get(nick)).map((id) -> new User(id, nick));
    }
}
