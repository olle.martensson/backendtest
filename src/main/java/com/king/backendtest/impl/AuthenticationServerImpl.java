package com.king.backendtest.impl;

import com.king.backendtest.api.AuthenticationServer;
import com.king.backendtest.api.ScoreServer;
import com.king.backendtest.api.UserServer;
import com.king.backendtest.domain.InvalidTokenException;
import com.king.backendtest.domain.NoSuchUserException;
import com.king.backendtest.domain.User;
import com.king.backendtest.util.Logging;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class AuthenticationServerImpl implements AuthenticationServer {
    private UserServer userServer;
    private ScoreServer scoreServer;
    private Map<String, User> activeTokens = new ConcurrentHashMap<>();

    public AuthenticationServerImpl(UserServer userServer, ScoreServer scoreServer) {
        this.userServer = userServer;
        this.scoreServer = scoreServer;
    }

    @Override
    public void authenticate(String nick) throws NoSuchUserException {
        User user = this.userServer.getUserByNick(nick).orElseThrow(() -> new NoSuchUserException(nick));

        String token = generateToken();
        this.activeTokens.put(token, user);
        sendTokenViaSomeTransport(token);
    }

    @Override
    public Optional<String> verify(String token) throws InvalidTokenException {
        Optional<User> user = Optional.ofNullable(this.activeTokens.get(token));
        return user.map((u) -> scoreServer.getSessionKey(u.getId()));
    }

    private static String generateToken() {
        return UUID.randomUUID().toString();
    }

    private static void sendTokenViaSomeTransport(String token) {
        Logging.logInfo(() -> String.format("Sending token: %s", token));
    }
}
