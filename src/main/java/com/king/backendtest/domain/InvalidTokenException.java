package com.king.backendtest.domain;

public class InvalidTokenException extends Exception {
    public InvalidTokenException() {
        super("The given token was not valid");
    }
}
