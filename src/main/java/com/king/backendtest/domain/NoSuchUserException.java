package com.king.backendtest.domain;

public class NoSuchUserException extends Exception {
    public NoSuchUserException(String nick) {
        super(String.format("A user represented by the nick: %s was not found in the system", nick));
    }
}
