package com.king.backendtest.domain;

import java.util.Objects;

public class User {
    private int id;
    private String nick;

    public User(int id, String nick) {
        this.id = id;
        this.nick = nick;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                nick.equals(user.nick);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nick);
    }
}
