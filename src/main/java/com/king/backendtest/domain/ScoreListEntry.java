package com.king.backendtest.domain;

import java.util.Objects;

public class ScoreListEntry {
    private int userId;
    private int score;

    public ScoreListEntry(int userId, int score) {
        this.userId = userId;
        this.score = score;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScoreListEntry that = (ScoreListEntry) o;
        return userId == that.userId &&
                score == that.score;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, score);
    }
}
