package com.king.backendtest.api;

/**
 * Handler for all servers in the system
 */
public interface System {

    ScoreServer getScoreServer();

    AuthenticationServer getAuthenticationServer();

    void stop();
}
