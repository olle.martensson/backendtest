package com.king.backendtest.api;

import com.king.backendtest.domain.InvalidTokenException;
import com.king.backendtest.domain.NoSuchUserException;

import java.util.Optional;

/**
 * An authentication server that authenticates by sending token
 * to a trusted device
 */
public interface AuthenticationServer {

    /**
     * Send authentication token to users trusted device
     * @param nick The unique nick representing the user
     * @throws NoSuchUserException when the given nick was not for a registered user
     */
    void authenticate(String nick) throws NoSuchUserException;

    /**
     * Verify the authentication token and return a session key from {@link ScoreServer#getSessionKey(int)}
     * @param token A token sent out by {@link #authenticate(String)}
     * @return A session key that can be used when calling the API
     * @throws InvalidTokenException Thrown when the token is not valid
     */
    Optional<String> verify(String token) throws InvalidTokenException;
}
