package com.king.backendtest.api;

public interface Stoppable {
    public void stop();
}
