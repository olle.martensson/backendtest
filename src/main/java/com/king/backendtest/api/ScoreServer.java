package com.king.backendtest.api;

import com.king.backendtest.domain.ScoreListEntry;

/**
 * A mini game backend which registers games scores for
 * different users and levels, with the capability to
 * return high score lists per level
 */
public interface ScoreServer {

    /**
     * Get a user session key that is valid for 10min
     * @param playerId The id of the user starting a session
     * @return A session key without spaces or "strange" characters
     */
    String getSessionKey(int playerId);

    /**
     * Retrieves the high scores for a specific level
     * @param level The level to retrieve scores for
     * @return A list of {@link ScoreListEntry} that represents the highest scores for the given level
     */
    ScoreListEntry[] getTopList(int level);

    /**
     * Register a new score for a player identified by the sessionKey
     * @param sessionKey A session key see {@link #getSessionKey}
     * @param level The level to register score for
     * @param score The score to register for the given level
     */
    void registerScore(String sessionKey, int level, int score);
}
