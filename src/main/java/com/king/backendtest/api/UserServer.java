package com.king.backendtest.api;

import com.king.backendtest.domain.NoSuchUserException;
import com.king.backendtest.domain.User;

import java.util.Optional;

/**
 * Handles the users of the game
 */
public interface UserServer {

    /**
     * Retrieve a {@link User} object from the system
     * @param nick The unique nickname representing the user
     * @return a {@link Optional<User>} representing the registered user
     */
    Optional<User> getUserByNick(String nick) throws NoSuchUserException;
}
