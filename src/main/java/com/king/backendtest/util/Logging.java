package com.king.backendtest.util;

import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Logging {

    public static void logWarning(Supplier<String> msgSupplier) {
        Logger.getGlobal().log(Level.INFO, msgSupplier);
    }

    public static void logInfo(Supplier<String> msgSupplier) {
        Logger.getGlobal().log(Level.INFO, msgSupplier);
    }
}
